"""
Extend xarray to deal with coastal water data along a line in space.
"""

import cartopy.crs as ccrs
import hvplot.pandas 
import hvplot.xarray
import geopandas as gpd
import geoviews as gv
import numpy as np
import pandas as pd
import shapely.geometry
import scipy.signal
import scipy.stats
import wavespectra
import xarray as xr

from pycoastalwater.basedataset import BaseDataset


@xr.register_dataset_accessor("line")
class LineDataset(BaseDataset):
    """
    Extend xarray's Dataset to deal with coastal water data along a line in space.

    The xarray.Dataset to extend must contain the followings :

    :Data dimensions:
    (x, time)

    :Data coordinates:
    time : xarray.DataArray
        Time data. The dimension is (time). 
        Warning : interval time recording must be constant.
    xm : xarray.DataArray
        The metric data along the line (optional). The dimension is (x).
    lon : xarray.DataArray
        The longitude data (optional). The dimensions is (x).
    lat : xarray.DataArray
        The latitude data (optional). The dimensions is (x).
    
    :Data variables:
    bathy : xarray.DataArray
        The bathymetry data. The dimensions is (x).
    h : xarray.DataArray
        The surface elevation h data. The dimensions are (time, x).
    u : xarray.DataArray
        The u velocity component data (optional). The dimensions are (time, x). 
        Not yet implemented.
    v : xarray.DataArray
        The v velocity component data (optional). The dimension are (time, x). 
        Not yet implemented.

    Example
    -------
    .. jupyter-execute::

        import pycoastalwater
        
        ds = pycoastalwater.tutorial.load_dataset("line")
        print(ds)
        print(ds.line) 
    """

    def __init__(self, xdataset_obj):
        super().__init__(xdataset_obj)

    def hig(self, df=0.005, fmin=0.003, fmax=0.03):
        """ 
        Sea surface infragravity wave significant height.
        
        Computed based on a power density spectrum and a targetted frequency range.

        Parameters
        ----------
        df : float
            The df frequency required to compute the power density spectrum. The 
            default value is 0.005 Hz.
        fmin : float
            The lowest cut-off frequency. The default value is 0.003 Hz.
        fmax : float
            The highest cut-off frequency. The default value is 0.03 Hz.

        Returns
        -------
        ds : xarray.Dataset
            The coastal water dataset with the hig data.
        
        Example
        -------
        .. jupyter-execute::

            import pycoastalwater
            
            ds = pycoastalwater.tutorial.load_dataset("line")
            ds.line.hig()
        """
        return super().hig(df, fmin, fmax)(coords=["x"])
    
    def hmax(self):
        """
        Sea surface wave maximum height.
        
        Computed based on the maximum value of h.

        Returns
        -------
        ds : xarray.Dataset
            The coastal water dataset with the hmax data.
        
        Example
        -------
        .. jupyter-execute::

            import pycoastalwater
            
            ds = pycoastalwater.tutorial.load_dataset("line")
            ds.line.hmax()
        """
        return super().hmax(coords=["x"])
        
    def hs(self, df=0.005, fmin=0.005, fmax=2):
        """
        Sea surface wave significant height.
        
        Computed based on a power density spectrum and a targetted frequency range.

        Parameters
        ----------
        df : float
            The df frequency required to compute the power density spectrum. The 
            default value is 0.005 Hz.
        fmin : float
            The lowest cut-off frequency. The default value is 0.005 Hz.
        fmax : float
            The highest cut-off frequency. The default value is 2 Hz.

        Returns
        -------
        ds : xarray.Dataset
            The coastal water dataset with the hs data.
        
        Example
        -------
        .. jupyter-execute::

            import pycoastalwater
            
            ds = pycoastalwater.tutorial.load_dataset("line")
            ds.line.hs()
        """
        return super().hs(df, fmin, fmax)(coords=["x"])

    def hsw(self, df=0.005, fmin=0.03, fmax=2):
        """
        Sea surface short wave significant height.
        
        Computed based on a power density spectrum and a targetted frequency range.

        Parameters
        ----------
        df : float
            The df frequency required to compute the power density spectrum. The 
            default value is 0.005 Hz.
        fmin : float
            The lowest cut-off frequency. The default value is 0.03 Hz.
        fmax : float
            The highest cut-off frequency. The default value is 2 Hz.

        Returns
        -------
        ds : xarray.Dataset
            The coastal water dataset with the hsw data.
        
        Example
        -------
        .. jupyter-execute::

            import pycoastalwater
            
            ds = pycoastalwater.tutorial.load_dataset("line")
            ds.line.hsw()
        """
        return super().hsw(df, fmin, fmax)(coords=["x"])

    def ef(self, df=0.005):
        """
        Sea surface wave directional variance spectral density based on the Welch's method.

        Parameters
        ----------
        df : float
            The df frequency required to compute the power density spectrum. The 
            default value is 0.005 Hz.

        Returns
        -------
        ds : xarray.Dataset
            The coastal water dataset with the ef data.
        
        Example
        -------
        .. jupyter-execute::

            import pycoastalwater
            
            ds = pycoastalwater.tutorial.load_dataset("line")
            ds.line.ef()
        """
        return super().ef(df, coords=["x"])
    
    def ku(self):
        """
        Sea surface kurtosis.
        
        Returns
        -------
        ds : xarray.Dataset
            The coastal water dataset with the ku data.
        
        Example
        -------
        .. jupyter-execute::

            import pycoastalwater
            
            ds = pycoastalwater.tutorial.load_dataset("line")
            ds.line.ku()
        """
        return super().ku(coords=["x"])
    
    def map(self, datavar, tile=gv.tile_sources.EsriImagery, **kwargs):
        """
        Plot data with holoview on a map.

        Parameters
        ----------
        datavar : string
            The name of the data of the coastal water object to plot.
        tile : geoviews.tile_sources
            The tiles element represents a so called web mapping tile source, 
            which fetches tiles appropriate to the current zoom level. The 
            default value is the geoviews.tile_sources.EsriImagery.
            More tiles are availables : https://geoviews.org/gallery/bokeh/tile_sources.html.
        kwargs
            Additional keyword arguments to be passed to the 
            hvplot.quadmesh function.
        
        Returns
        -------
        plot : holoviews.core.overlay.Overlay
            The holoview plot.
        
        Example
        -------
        Plot the bathymetry on a map.

        .. jupyter-execute::

            import pycoastalwater

            ds = pycoastalwater.tutorial.load_dataset("line")
            ds.line.map("bathy", cmap="jet")
        
        Plot the sea surface height between 0 and 30 seconds on a map.

        .. jupyter-execute::

            ds.sel(time=slice(0, 30)).line.map("h")
        """

        # title
        try :
            title = self._obj[datavar].standard_name + r" [" + self._obj[datavar].units + r"]",
        except:
            title = [""]

        if self._obj[datavar].ndim == 1:

            # create shapely points
            points = [shapely.geometry.Point(lon, lat) for lon, lat in zip(self._obj['lon'].values.flatten(), self._obj['lat'].values.flatten())]

            # create a pandas DataFrame
            data = pd.DataFrame({'geometry': points, "values" : self._obj[datavar].values.flatten()})

            # convert the pandas DataFrame to a GeoDataFrame
            gdf = gpd.GeoDataFrame(data, geometry='geometry')

            # dictt
            dictt = {
                "crs": ccrs.PlateCarree(),
                "title": title[0],
                "c": "values",
            }

            # delete var from dictt if inside kwargs
            for key in kwargs:
                if key in dictt:
                    del dictt[key]

            # create plot
            plot = gdf.hvplot(
                **dictt,
                **kwargs,
                )

        elif self._obj[datavar].ndim == 2:

            # prepare data
            nt = len(self._obj['time'])
            nx = len(self._obj['x'])
            times = np.concatenate([np.tile(x, nx) for x in self._obj.time.data])
            lons = np.tile(self._obj['lon'].data, nt)
            lats = np.tile(self._obj['lat'].data, nt)
            
            # detect limits
            cmin, cmax = self._obj[datavar].min().data, self._obj[datavar].max().data 
       
            # create shapely points
            points = [shapely.geometry.Point(lon, lat) for lon, lat in zip(lons, lats)]

            # create a pandas DataFrame
            data = pd.DataFrame({
                'geometry': points,
                "values": self._obj[datavar].values.flatten(),
                "time": times,
                })

            # convert the pandas DataFrame to a GeoDataFrame
            gdf = gpd.GeoDataFrame(data, geometry='geometry')

            # dictt
            dictt = {
                "crs": ccrs.PlateCarree(),
                "title": title[0],
                "c": "values",
                "groupby": "time",
                "dynamic": False,
                "clim": (cmin, cmax),
            }

            # delete var from dictt if inside kwargs
            for key in kwargs:
                if key in dictt:
                    del dictt[key]

            # create plot
            plot = gdf.hvplot(
                **dictt,
                **kwargs,
                )

        # add map tile on plot
        plot = plot*tile

        return plot

    def num_peaks(self, **kwargs):
        """
        Sea surface number of peaks based on peak properties.
        
        Parameters
        ----------
        kwargs
            Additional keyword arguments to be passed to the 
            scipy.signal.find_peaks function.

        Returns
        -------
        ds : xarray.Dataset
            The coastal water dataset with the num_peaks data.
        
        Example
        -------
        .. jupyter-execute::

            import pycoastalwater
            
            ds = pycoastalwater.tutorial.load_dataset("line")
            ds.line.num_peaks()
        """        
        
        # copy obj
        ds = self._obj.copy()
    
        # define a function to find the number of peaks in a 1D array
        def find_num_peaks(arr):
            peaks, _ = scipy.signal.find_peaks(arr, **kwargs)
            return len(peaks)
    
        # apply the function to every cell in the 2D xarray object
        num_peaks = xr.apply_ufunc(
            find_num_peaks, ds.h, input_core_dims=[['time']], vectorize=True,
            )
        
        # add num_peaks as var
        ds = ds.assign({"num_peaks": (["x"], num_peaks.data,)})
        ds.num_peaks.attrs['standard_name'] = "sea_surface_number_of_peaks"
        ds.num_peaks.attrs['units'] = "occurrences"

        # float 16
        # ds = ds.astype('float16')

        return ds

    def plot(self, datavar, plot_type="line", **kwargs):
        """
        Plot data with holoview.

        Parameters
        ----------
        datavar : string
            The name of the data of the coastal water object to plot.
        plot_type : string
            The name of the type of the plot {"line", "scatter"}. 
            The default value is "line".
        kwargs
            Additional keyword arguments to be passed to the 
            hvplot.quadmesh function.
        
        Returns
        -------
        plot : holoviews.core.overlay.Overlay
            The holoview plot.
        
        Example
        -------
        Plot the bathymetry.

        .. jupyter-execute::

            import pycoastalwater

            ds = pycoastalwater.tutorial.load_dataset("line")
            ds.line.plot("bathy", x="xm")
        
        Plot the sea surface height between 0 and 30 seconds.

        .. jupyter-execute::

            ds.sel(time=slice(0, 30)).line.plot("h", x="xm")
        
        Add the bathymetry.

        .. jupyter-execute::

            ds.sel(time=slice(0, 30)).line.plot("h", x="xm", ylim=(-15, 10)) * \
                ds.line.plot("bathy", x="xm", ylim=(-15, 10), color="grey")
        """
        
        # detect color limits
        ymin = float(self._obj[datavar].min().data)
        ymax = float(self._obj[datavar].max().data)
          
        # dictt
        dictt = {
            "dynamic": False,
            "ylim": (ymin, ymax),
        }

        # delete var from dictt if inside kwargs
        for key in kwargs:
            if key in dictt:
                del dictt[key]

        if plot_type == "line":

            # plot
            plot = self._obj[datavar].hvplot.line(
                **dictt,
                **kwargs,
                )
            
        elif plot_type == "scatter":

            # plot
            plot = self._obj[datavar].hvplot.scatter(
                **dictt,
                **kwargs,
                )

        return plot

    def runup(self):
        """
        Sea surface runup positions.
        
        The values are the associated surface elevation water values.
        
        Returns
        -------
        ds : xarray.Dataset
            The coastal water dataset with the runup data.
        
        Example
        -------
        .. jupyter-execute::

            import pycoastalwater
            
            ds = pycoastalwater.tutorial.load_dataset("line")
            ds.line.runup()
        """

        # copy obj
        ds = self._obj.copy()
    
        # find x indice of first nan
        xind = ds.h.isnull().argmax(axis=1).data
        
        # minus one indice
        xind = xind-1

        # initialize runup
        r = ds.h.data*np.nan

        # fill runup
        for i_t in range(len(ds.time.data)):
                r[i_t, xind[i_t]] = ds.h.data[i_t, xind[i_t]]

        # add runup as var
        ds = ds.assign({"runup": (["time", "x"], r,)})
        ds.runup.attrs['standard_name'] = "sea_surface_runup_position"
        ds.runup.attrs['units'] = "m"

        # float 16
        # ds = ds.astype('float16')

        return ds
    
    def runup_q(self, q):
        """
        Sea surface runup positions qth quantile.
        
        The values are the associated bathymetry elevation values.
        
        Parameters
        ----------
        q : float
            The qth quantile to compute, which must be between 0 and 1 inclusive.
        
        Returns
        -------
        ds : xarray.Dataset
            The coastal water dataset with the runup_q data.
        
        Example
        -------
        .. jupyter-execute::

            import pycoastalwater
            
            ds = pycoastalwater.tutorial.load_dataset("line")
            ds.line.runup_q(q=0.75)
        """
         
        # copy obj
        ds = self._obj.copy()
    
        # find x indice of first nan
        xind = ds.h.isnull().argmax(axis=1).data
        
        # minus one indice
        xind = xind-1
        
        # quantile
        xind = np.quantile(xind, q=q, axis=0)    

        # round
        xind = np.round(xind).astype(int)

        # initialize runup
        rq = ds.bathy.data*np.nan

        # fill runup_quantile
        rq[xind] = ds.bathy.data[xind]

        # add runup as vars
        ds = ds.assign({"runup_q": (["x"], rq,)})
        ds.runup_q.attrs['standard_name'] = "sea_surface_runup_position_quantile"
        ds.runup_q.attrs['units'] = "m"

        # float 16
        # ds = ds.astype('float16')

        return ds
         
    def setup(self, wl):
        """
        Sea surface water setup.
        
        Parameters
        ----------
        wl : float
            The water level reference, which serve as reference to 
            compute the setup.

        Returns
        -------
        ds : xarray.Dataset
            The coastal water dataset with the wl data.
        
        Example
        -------
        .. jupyter-execute::

            import pycoastalwater
            
            ds = pycoastalwater.tutorial.load_dataset("line")
            ds.line.setup(wl=4)
        """
        return super().setup(wl)

    def sk(self):
        """
        Sea surface water skewness.
        
        Returns
        -------
        ds : xarray.Dataset
            The coastal water dataset with the sk data.
        
        Example
        -------
        .. jupyter-execute::

            import pycoastalwater
            
            ds = pycoastalwater.tutorial.load_dataset("line")
            ds.line.sk()
        """
        return super().sk(coords=["x"])
    
    def tp(self, df=0.005, fmin=0.005, fmax=2):
        """
        Sea surface wave period at variance spectral density maximum.
        
        Computed based on a power density spectrum and a targetted frequency range.

        Parameters
        ----------
        df : int|float
            The df frequency required to compute the power density spectrum. The 
            default value is 0.005 Hz.
        fmin : int|float
            The lowest cut-off frequency. The default value is 0.005 Hz.
        fmax_out : int|float
            The highest cut-off frequency. The default value is 2 Hz.

        Returns
        -------
        ds : xarray.Dataset
            The coastal water dataset with the tp data.
        
        Example
        -------
        .. jupyter-execute::

            import pycoastalwater
            
            ds = pycoastalwater.tutorial.load_dataset("line")
            ds.line.tp()
        """
        return super().tp(df, fmin, fmax)(coords=["x"])
    
    def tm01(self, df=0.005, fmin=0.005, fmax=2):
        """
        Sea surface wave mean period.
        
        Computed from variance spectral density first frequency moment based 
        on a power density spectrum and a targetted frequency range.

        Parameters
        ----------
        df : float
            The df frequency required to compute the power density spectrum. The 
            default value is 0.005 Hz.
        fmin : float
            The lowest cut-off frequency. The default value is 0.005 Hz.
        fmax : float
            The highest cut-off frequency. The default value is 2 Hz.

        Returns
        -------
        ds : xarray.Dataset
            The coastal water dataset with the tm01 data.
        
        Example
        -------
        .. jupyter-execute::

            import pycoastalwater
            
            ds = pycoastalwater.tutorial.load_dataset("line")
            ds.line.tm01()
        """
        return super().tm01(df, fmin, fmax)(coords=["x"])
    
    def tm02(self, df=0.005, fmin=0.005, fmax=2):
        """
        Sea surface wave mean period.
        
        Computed from variance spectral density second frequency moment based 
        on a power density spectrum and a targetted frequency range.

        Parameters
        ----------
        df : float
            The df frequency required to compute the power density spectrum. The 
            default value is 0.005 Hz.
        fmin : float
            The lowest cut-off frequency. The default value is 0.005 Hz.
        fmax_out : float
            The highest cut-off frequency. The default value is 2 Hz.

        Returns
        -------
        ds : xarray.Dataset
            The coastal water dataset with the tm02 data.
        
        Example
        -------
        .. jupyter-execute::

            import pycoastalwater
            
            ds = pycoastalwater.tutorial.load_dataset("line")
            ds.line.tm02()
        """
        return super().tm02(df, fmin, fmax)(coords=["x"])

    def ws(self):
        """
        Sea surface water sheet crest height.
        
        Returns
        -------
        ds : xarray.Dataset
            The coastal water dataset with the ws data.
        
        Example
        -------
        .. jupyter-execute::

            import pycoastalwater
            
            ds = pycoastalwater.tutorial.load_dataset("line")
            ds.line.ws()
        """
        return super().ws()

    def ws_q(self, q):
        """
        Sea surface water sheet crest height qth quantile.
        
        Parameters
        ----------
        q : float
            The qth quantile to compute, which must be between 0 and 1 inclusive.

        Returns
        -------
        ds : xarray.Dataset
            The coastal water dataset with the ws_q data.
        
        Example
        -------
        .. jupyter-execute::

            import pycoastalwater
            
            ds = pycoastalwater.tutorial.load_dataset("line")
            ds.line.ws_q(q=0.75)
        """
        return super().ws_q(q, coords=["x"])
