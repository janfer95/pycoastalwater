"""
Python module to create data for pycoastalwater tutorial. 
"""

import pathlib
import xarray as xr

def load_dataset(name):
    """
    Load xarray dataset for pycoastalwater tutorial.

    Parameters
    ----------
    name : str
        The name of the dataset to load. 
        The values are {"grid", "line", "point"}.

    Returns
    -------
    ds : xarray.Dataset
        The xarray dataset with the data.
    """

    # The data have been created using :
    # ds= pybarracuda.post.results_to_xarray(
    #    r"/home/aurelien/Documents/GIT/2d/simu/results",
    #    sel_time=slice(400, 2400),
    #    sel_xm= slice(1500, 2500, 5),
    #    sel_ym = slice(1100, 2600, 5),
    #    lon=lon,
    #    lat=lat,
    #    wd_th=0.1,
    #    wl=4,
    #    )
    # ds.attrs["description"] = "Tutorial data"
    # ds["time"] = ds["time"] - 400  
    # ds['time'].attrs['standard_name'] = 'time'       
    # ds['time'].attrs['units'] = 's' 

    p_data = str(pathlib.Path(__file__).parent.resolve())

    if name == "grid":

        ds = xr.open_dataset(p_data + r"/data/biarritz.nc")
    
    elif name == "line":
    
        ds = xr.open_dataset(p_data + r"/data/biarritz.nc")
        ds = ds.isel(y=30)
        ds = ds.drop(["y", "ym"])

    elif name == "point":
        
        ds = xr.open_dataset(p_data + r"/data/biarritz.nc")
        ds = ds.isel(x=10, y=30)
        ds = ds.drop(["x", "y", "xm", "ym"])
            
    return ds