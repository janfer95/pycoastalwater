"""
Extend xarray to deal with coastal water data along a grid in space.
"""

import cartopy.crs as ccrs
from colorcet import rainbow4
import datashader.transfer_functions as tf
import hvplot.xarray
import geoviews as gv
import numpy as np
import plotly.graph_objects as go
import scipy.signal
import scipy.stats
import wavespectra
import xarray as xr

from pycoastalwater.basedataset import BaseDataset


@xr.register_dataset_accessor("grid")
class GridDataset(BaseDataset):
    """
    Extend xarray's Dataset to deal with coastal water data along a grid in space.

    The xarray.Dataset to extend must contain the followings :

    :Data dimensions:
    (y, x, time)

    :Data coordinates:
    time : xarray.DataArray
        Time data. The dimension is (time). 
        Warning : interval time recording must be constant.
    xm : xarray.DataArray
        The metric data along the cross-shore axis (optional). The dimension is (x).
    ym : xarray.DataArray
        The metric data along the longshore axis (optional). The dimension is (y).    
    lon : xarray.DataArray
        The longitude 2D gridded data (optional). The dimensions are (y, x).
    lat : xarray.DataArray
        The latitude 2D gridded data (optional). The dimensions are (y, x).
    
    :Data variables:
    bathy : xarray.DataArray
        The bathymetry data. The dimensions are (y, x).
    h : xarray.DataArray
        The surface elevation h data. The dimensions are (time, y, x).
    u : xarray.DataArray
        The u velocity component data (optional). The dimensions are (time, y, x). 
        Not yet implemented.
    v : xarray.DataArray
        The v velocity component data (optional). The dimension are (time, y, x). 
        Not yet implemented.

    Example
    -------
    .. jupyter-execute::
        
        import pycoastalwater

        ds = pycoastalwater.tutorial.load_dataset("grid")
        print(ds)
        print(ds.grid)
    
    To do
    -----
    add map_image_animation

    """

    def __init__(self, xdataset_obj):
        super().__init__(xdataset_obj)

    def hig(self, df=0.005, fmin=0.003, fmax=0.03):
        """ 
        Sea surface infragravity wave significant height.
        
        Computed based on a power density spectrum and a targetted frequency range.

        Parameters
        ----------
        df : float
            The df frequency required to compute the power density spectrum. The 
            default value is 0.005 Hz.
        fmin : float
            The lowest cut-off frequency. The default value is 0.003 Hz.
        fmax : float
            The highest cut-off frequency. The default value is 0.03 Hz.

        Returns
        -------
        ds : xarray.Dataset
            The coastal water dataset with the hig data.
        
        Example
        -------
        .. jupyter-execute::

            import pycoastalwater
            
            ds = pycoastalwater.tutorial.load_dataset("grid")
            ds.grid.hig()
        """
        return super().hig(df, fmin, fmax)(coords=["y", "x"])
    
    def hmax(self):
        """
        Sea surface wave maximum height.
        
        Computed based on the maximum value of h.

        Returns
        -------
        ds : xarray.Dataset
            The coastal water dataset with the hmax data.
        
        Example
        -------
        .. jupyter-execute::

            import pycoastalwater
            
            ds = pycoastalwater.tutorial.load_dataset("grid")
            ds.grid.hmax()
        """
        return super().hmax(coords=["y", "x"])
        
    def hs(self, df=0.005, fmin=0.005, fmax=2):
        """
        Sea surface wave significant height.
        
        Computed based on a power density spectrum and a targetted frequency range.

        Parameters
        ----------
        df : float
            The df frequency required to compute the power density spectrum. The 
            default value is 0.005 Hz.
        fmin : float
            The lowest cut-off frequency. The default value is 0.005 Hz.
        fmax : float
            The highest cut-off frequency. The default value is 2 Hz.

        Returns
        -------
        ds : xarray.Dataset
            The coastal water dataset with the hs data.
        
        Example
        -------
        .. jupyter-execute::

            import pycoastalwater
            
            ds = pycoastalwater.tutorial.load_dataset("grid")
            ds.grid.hs()
        """
        return super().hs(df, fmin, fmax)(coords=["y", "x"])

    def hsw(self, df=0.005, fmin=0.03, fmax=2):
        """
        Sea surface short wave significant height.
        
        Computed based on a power density spectrum and a targetted frequency range.

        Parameters
        ----------
        df : float
            The df frequency required to compute the power density spectrum. The 
            default value is 0.005 Hz.
        fmin : float
            The lowest cut-off frequency. The default value is 0.03 Hz.
        fmax : float
            The highest cut-off frequency. The default value is 2 Hz.

        Returns
        -------
        ds : xarray.Dataset
            The coastal water dataset with the hsw data.
        
        Example
        -------
        .. jupyter-execute::

            import pycoastalwater
            
            ds = pycoastalwater.tutorial.load_dataset("grid")
            ds.grid.hsw()
        """
        return super().hsw(df, fmin, fmax)(coords=["y", "x"])
    
    def ef(self, df=0.005):
        """
        Sea surface wave directional variance spectral density based on the Welch's method.

        Parameters
        ----------
        df : float
            The df frequency required to compute the power density spectrum. The 
            default value is 0.005 Hz.

        Returns
        -------
        ds : xarray.Dataset
            The coastal water dataset with the ef data.
        
        Example
        -------
        .. jupyter-execute::

            import pycoastalwater
            
            ds = pycoastalwater.tutorial.load_dataset("grid")
            ds.grid.ef()
        """
        return super().ef(df, coords=["y", "x"])
    
    def ku(self):
        """
        Sea surface kurtosis.
        
        Returns
        -------
        ds : xarray.Dataset
            The coastal water dataset with the ku data.
        
        Example
        -------
        .. jupyter-execute::

            import pycoastalwater
            
            ds = pycoastalwater.tutorial.load_dataset("grid")
            ds.grid.ku()
        """
        return super().ku(coords=["y", "x"])
    
    def map(self, datavar, tile=gv.tile_sources.EsriImagery, **kwargs):
        """
        Plot data with holoview on a map.

        Parameters
        ----------
        datavar : string
            The name of the data of the coastal water object to plot.
        tile : geoviews.tile_sources
            The tiles element represents a so called web mapping tile source, 
            which fetches tiles appropriate to the current zoom level. The 
            default value is the geoviews.tile_sources.EsriImagery.
            More tiles are availables : https://geoviews.org/gallery/bokeh/tile_sources.html.
        kwargs
            Additional keyword arguments to be passed to the 
            hvplot.quadmesh function.
        
        Returns
        -------
        plot : holoviews.core.overlay.Overlay
            The holoview plot.
        
        Example
        -------
        Plot the bathymetry on a map.

        .. jupyter-execute::

            import pycoastalwater

            ds = pycoastalwater.tutorial.load_dataset("grid")
            ds.grid.map("bathy", cmap="jet")
        
        Plot the sea surface height between 0 and 30 seconds on a map.

        .. jupyter-execute::

            ds.sel(time=slice(0, 30)).grid.map("h")
        """

        # title
        title = self._obj[datavar].standard_name + r" [" + self._obj[datavar].units + r"]",

        # dictt
        dictt = {
            "x": 'lon',
            "y": 'lat',
            "crs": ccrs.PlateCarree(),
            "title": title[0],
            
        }

        # delete var from dictt if inside kwargs
        for key in kwargs:
            if key in dictt:
                del dictt[key]

        # create plot
        plot = self.plot(
            datavar=datavar,
            **dictt,
            **kwargs,
            )

        # add map tile on plot
        plot = plot*tile

        return plot
    
    def map_image(self, datavar="h", **kwargs):
        """
        Plot data as an image with plotly on a map.

        Parameters
        ----------
        datavar : string
            The name of the data of the coastal water object to plot.
        kwargs
            Additional keyword arguments to be passed to the 
            hvplot.quadmesh function.
        
        Returns
        -------
        fig : plotly.graph_objects.Figure
            The plotly figure.

        Example
        -------
        .. jupyter-execute::

            import pycoastalwater
            
            ds = pycoastalwater.tutorial.load_dataset("grid")
            ds.grid.map_image(datavar="bathy")
        """

        ds_plot = self._obj[datavar]
        ds_plot = ds_plot.astype("float32")

        span = [np.nanmin(ds_plot), np.nanmax(ds_plot)]

        img = tf.shade(ds_plot, how='linear', cmap=rainbow4, span=span).to_pil()
        coor = [[ds_plot.lon.data[-1,0], ds_plot.lat.data[-1,0]],
                [ds_plot.lon.data[-1,-1], ds_plot.lat.data[-1,-1]],
                [ds_plot.lon.data[0,-1], ds_plot.lat.data[0,-1]],
                [ds_plot.lon.data[0,0], ds_plot.lat.data[0,0]]]

        fig = go.Figure()

        fig.add_trace(
                go.Scattermapbox(
                        lon=[None],
                        lat=[None],
                        mode='markers',
                        marker={'color':[0],
                                'coloraxis':'coloraxis'},
                        showlegend=False))
        
        # title
        title = self._obj[datavar].standard_name + r" [" + self._obj[datavar].units + r"]",

        fig.update_layout(
            # map parameters
            mapbox={
                'style':'stamen-terrain',
                'center':{'lon':np.mean([c[0] for c in coor]),'lat':np.mean([c[1] for c in coor])},
                'zoom':10,
                # adding the image to the figure
                'layers':[{
                    'sourcetype':'image',
                    'source':img,
                    'coordinates':coor,
                    'below':'traces'
                }]
            },
            # colorbar parameters
            coloraxis={
                'cmax':span[1],
                'cmin':span[0],
                'colorscale':rainbow4,
                'colorbar':{
                        'orientation':'h',
                        'y':-0.15
                        }
            },
            # title
            #title = title,
        )

        return fig

    def num_peaks(self, **kwargs):
        """
        Sea surface number of peaks based on peak properties.
        
        Parameters
        ----------
        kwargs
            Additional keyword arguments to be passed to the 
            scipy.signal.find_peaks function.

        Returns
        -------
        ds : xarray.Dataset
            The coastal water dataset with the num_peaks data.
        
        Example
        -------
        .. jupyter-execute::

            import pycoastalwater
            
            ds = pycoastalwater.tutorial.load_dataset("grid")
            ds.grid.num_peaks()
        """        
        
        # copy obj
        ds = self._obj.copy()
    
        # define a function to find the number of peaks in a 1D array
        def find_num_peaks(arr):
            peaks, _ = scipy.signal.find_peaks(arr, **kwargs)
            return len(peaks)
    
        # apply the function to every cell in the 2D xarray object
        num_peaks = xr.apply_ufunc(
            find_num_peaks, ds.h, input_core_dims=[['time']], vectorize=True,
            )
        
        # add num_peaks as var
        ds = ds.assign({"num_peaks": (["y", "x"], num_peaks.data,)})
        ds.num_peaks.attrs['standard_name'] = "sea_surface_number_of_peaks"
        ds.num_peaks.attrs['units'] = "occurrences"

        # float 16
        # ds = ds.astype('float16')

        return ds

    def plot(self, datavar, **kwargs):
        """
        Plot data with holoview.

        Parameters
        ----------
        datavar : string
            The name of the data of the coastal water object to plot.
        kwargs
            Additional keyword arguments to be passed to the 
            hvplot.quadmesh function.
        
        Returns
        -------
        plot : holoviews.core.overlay.Overlay
            The holoview plot.
        
        Example
        -------
        Plot the bathymetry.

        .. jupyter-execute::

            import pycoastalwater

            ds = pycoastalwater.tutorial.load_dataset("grid")
            ds.grid.plot("bathy", x="xm", y="ym", cmap="jet")
        
        Plot the sea surface height between 0 and 30 seconds.

        .. jupyter-execute::

            ds.sel(time=slice(0, 30)).grid.plot("h", x="lon", y="lat")
        """
        
        # detect color limits
        cmin, cmax = self._obj[datavar].min().data, self._obj[datavar].max().data 
          
        # dictt
        dictt = {
            "dynamic": False,
            "clim": (cmin, cmax),
        }

        # delete var from dictt if inside kwargs
        for key in kwargs:
            if key in dictt:
                del dictt[key]

        # plot
        plot = self._obj[datavar].hvplot.quadmesh(
            **dictt,
            **kwargs,
            )
        
        return plot

    def runup(self):
        """
        Sea surface runup positions.
        
        The values are the associated surface elevation water values.
        
        Returns
        -------
        ds : xarray.Dataset
            The coastal water dataset with the runup data.
        
        Example
        -------
        .. jupyter-execute::

            import pycoastalwater
            
            ds = pycoastalwater.tutorial.load_dataset("grid")
            ds.grid.runup()
        """

        # copy obj
        ds = self._obj.copy()
    
        # find x indice of first nan
        xind = ds.h.isnull().argmax(axis=2).data
        
        # minus one indice
        xind = xind-1

        # initialize runup
        r = ds.h.data*np.nan

        # fill runup
        for i_t in range(len(ds.time.data)):
            for i_y in range(len(ds.y.data)):
                r[i_t, i_y, xind[i_t, i_y]] = ds.h.data[i_t, i_y, xind[i_t, i_y]]

        # add runup as var
        ds = ds.assign({"runup": (["time", "y", "x"], r,)})
        ds.runup.attrs['standard_name'] = "sea_surface_runup_position"
        ds.runup.attrs['units'] = "m"

        # float 16
        # ds = ds.astype('float16')

        return ds
    
    def runup_q(self, q):
        """
        Sea surface runup positions qth quantile.
        
        The values are the associated bathymetry elevation values.
        
        Parameters
        ----------
        q : float
            The qth quantile to compute, which must be between 0 and 1 inclusive.
        
        Returns
        -------
        ds : xarray.Dataset
            The coastal water dataset with the runup_q data.
        
        Example
        -------
        .. jupyter-execute::

            import pycoastalwater
            
            ds = pycoastalwater.tutorial.load_dataset("grid")
            ds.grid.runup_q(q=0.75)
        """
         
        # copy obj
        ds = self._obj.copy()
    
        # find x indice of first nan
        xind = ds.h.isnull().argmax(axis=2).data
        
        # minus one indice
        xind = xind-1
        
        # quantile
        xind = np.quantile(xind, q=q, axis=0)    

        # round
        xind = np.round(xind).astype(int)

        # initialize runup
        rq = ds.bathy.data*np.nan

        # fill runup_quantile
        for i_y in range(len(ds.y.data)):
            rq[i_y, xind[i_y]] = ds.bathy.data[i_y, xind[i_y]]

        # add runup as vars
        ds = ds.assign({"runup_q": (["y", "x"], rq,)})
        ds.runup_q.attrs['standard_name'] = "sea_surface_runup_position_quantile"
        ds.runup_q.attrs['units'] = "m"

        # float 16
        # ds = ds.astype('float16')

        return ds
         
    def setup(self, wl):
        """
        Sea surface setup.
        
        Parameters
        ----------
        wl : float
            The water level reference, which serve as reference to 
            compute the setup.

        Returns
        -------
        ds : xarray.Dataset
            The coastal water dataset with the wl data.
        
        Example
        -------
        .. jupyter-execute::

            import pycoastalwater
            
            ds = pycoastalwater.tutorial.load_dataset("grid")
            ds.grid.setup(wl=4)
        """
        return super().setup(wl)

    def sk(self):
        """
        Sea surface skewness.
        
        Returns
        -------
        ds : xarray.Dataset
            The coastal water dataset with the sk data.
        
        Example
        -------
        .. jupyter-execute::

            import pycoastalwater
            
            ds = pycoastalwater.tutorial.load_dataset("grid")
            ds.grid.sk()
        """
        return super().sk(coords=["y", "x"])
    
    def tp(self, df=0.005, fmin=0.005, fmax=2):
        """
        Sea surface wave period at variance spectral density maximum.
        
        Computed based on a power density spectrum and a targetted frequency range.

        Parameters
        ----------
        df : int|float
            The df frequency required to compute the power density spectrum. The 
            default value is 0.005 Hz.
        fmin : int|float
            The lowest cut-off frequency. The default value is 0.005 Hz.
        fmax_out : int|float
            The highest cut-off frequency. The default value is 2 Hz.

        Returns
        -------
        ds : xarray.Dataset
            The coastal water dataset with the tp data.
        
        Example
        -------
        .. jupyter-execute::

            import pycoastalwater
            
            ds = pycoastalwater.tutorial.load_dataset("grid")
            ds.grid.tp()
        """
        return super().tp(df, fmin, fmax)(coords=["y", "x"])
    
    def tm01(self, df=0.005, fmin=0.005, fmax=2):
        """
        Sea surface wave mean period.
        
        Computed from variance spectral density first frequency moment based 
        on a power density spectrum and a targetted frequency range.

        Parameters
        ----------
        df : float
            The df frequency required to compute the power density spectrum. The 
            default value is 0.005 Hz.
        fmin : float
            The lowest cut-off frequency. The default value is 0.005 Hz.
        fmax : float
            The highest cut-off frequency. The default value is 2 Hz.

        Returns
        -------
        ds : xarray.Dataset
            The coastal water dataset with the tm01 data.
        
        Example
        -------
        .. jupyter-execute::

            import pycoastalwater
            
            ds = pycoastalwater.tutorial.load_dataset("grid")
            ds.grid.tm01()
        """
        return super().tm01(df, fmin, fmax)(coords=["y", "x"])
    
    def tm02(self, df=0.005, fmin=0.005, fmax=2):
        """
        Sea surface wave mean period.
        
        Computed from variance spectral density second frequency moment based 
        on a power density spectrum and a targetted frequency range.

        Parameters
        ----------
        df : float
            The df frequency required to compute the power density spectrum. The 
            default value is 0.005 Hz.
        fmin : float
            The lowest cut-off frequency. The default value is 0.005 Hz.
        fmax_out : float
            The highest cut-off frequency. The default value is 2 Hz.

        Returns
        -------
        ds : xarray.Dataset
            The coastal water dataset with the tm02 data.
        
        Example
        -------
        .. jupyter-execute::

            import pycoastalwater
            
            ds = pycoastalwater.tutorial.load_dataset("grid")
            ds.grid.tm02()
        """
        return super().tm02(df, fmin, fmax)(coords=["y", "x"])

    def ws(self):
        """
        Sea surface water sheet crest height.
        
        Returns
        -------
        ds : xarray.Dataset
            The coastal water dataset with the ws data.
        
        Example
        -------
        .. jupyter-execute::

            import pycoastalwater
            
            ds = pycoastalwater.tutorial.load_dataset("grid")
            ds.grid.ws()
        """
        return super().ws()

    def ws_q(self, q):
        """
        Crest height of the sea surface water sheet qth quantile.
        
        Parameters
        ----------
        q : float
            The qth quantile to compute, which must be between 0 and 1 inclusive.

        Returns
        -------
        ds : xarray.Dataset
            The coastal water dataset with the ws_q data.
        
        Example
        -------
        .. jupyter-execute::

            import pycoastalwater
            
            ds = pycoastalwater.tutorial.load_dataset("grid")
            ds.grid.ws_q(q=0.75)
        """
        return super().ws_q(q, coords=["y", "x"])
