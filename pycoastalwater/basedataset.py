"""
Extend xarray to deal with coastal water data along a grid in space.
"""

import cartopy.crs as ccrs
from colorcet import rainbow4
import datashader.transfer_functions as tf
import hvplot.xarray
import geoviews as gv
import numpy as np
import plotly.graph_objects as go
import scipy.signal
import scipy.stats
import wavespectra
import xarray as xr
import re

from functools import partial
from typing import Callable


class BaseDataset:
    """
    Extend xarray's Dataset to deal with coastal water data along a grid in space.

    The xarray.Dataset to extend must contain the followings :

    :Data dimensions:
    (y, x, time)

    :Data coordinates:
    time : xarray.DataArray
        Time data. The dimension is (time).
        Warning : interval time recording must be constant.
    xm : xarray.DataArray
        The metric data along the cross-shore axis (optional). The dimension is (x).
    ym : xarray.DataArray
        The metric data along the longshore axis (optional). The dimension is (y).
    lon : xarray.DataArray
        The longitude 2D gridded data (optional). The dimensions are (y, x).
    lat : xarray.DataArray
        The latitude 2D gridded data (optional). The dimensions are (y, x).

    :Data variables:
    bathy : xarray.DataArray
        The bathymetry data. The dimensions are (y, x).
    h : xarray.DataArray
        The surface elevation h data. The dimensions are (time, y, x).
    u : xarray.DataArray
        The u velocity component data (optional). The dimensions are (time, y, x).
        Not yet implemented.
    v : xarray.DataArray
        The v velocity component data (optional). The dimension are (time, y, x).
        Not yet implemented.
    """

    def __init__(self, xdataset_obj):
        self._obj = xdataset_obj

    def __getattr__(self, attr):
        return getattr(self._obj, attr)

    def __repr__(self):
        return re.sub(r"<.+>", f"<{self.__class__.__name__}>", str(self._obj))

    def __compute_ws_variable(
            self,
            df,
            fmin,
            fmax,
            ws_variable,
            out_variable,
            coords,
            standard_name,
            units
    ):
        """
        Method to avoid duplication when computing wavespectra variables

        Internally calls wavespectra functions to compute the variables.
        Avoids repeating by using getattr to dynamically call the right
        function.

        Parameters
        ----------
        df : float
            The df frequency required to compute the power density spectrum.
        fmin : float
            The lowest cut-off frequency.
        fmax : float
            The highest cut-off frequency.
        ws_variable : str
            Which variable to compute with wavespectra (e.g. 'hs', 'tm02',...)
        out_variable : str
            The name of the output variable. Note that this can, but does not
            have to be equal to ws_variable
        coords : Optional[list[str, ...]]
            Which and how many coordinates to use. For example ["y", "x"].
        standard_name : str
            Standard name of the variable
        units : str
            Units of the variable

        Returns
        -------
        ds : xarray.Dataset
            The coastal water dataset with the additional variable.
        """
        # compute spectra
        ds = self.ef(df=df)

        # frequencies cut-off
        ds = ds.sel(freq=slice(fmin, fmax))

        # hs using wavespectra
        da = getattr(ds.ef.spec, ws_variable)()

        # nan data
        da = da.where(da > 0, np.nan)

        # copy obj
        ds = self._obj.copy()

        # add hig as vars
        if coords is None:
            ds = ds.assign({out_variable: da.data})
        else:
            ds = ds.assign({out_variable: (coords, da.data,)})
        getattr(ds, out_variable).attrs['standard_name'] = standard_name
        getattr(ds, out_variable).attrs['units'] = units

        # float 16
        # ds = ds.astype('float16')

        return ds

    def hig(self, df=0.005, fmin=0.003, fmax=0.03) -> Callable:
        """
        Sea surface infragravity wave significant height.

        Computed based on a power density spectrum and a targetted frequency range.

        Parameters
        ----------
        df : float
            The df frequency required to compute the power density spectrum. The
            default value is 0.005 Hz.
        fmin : float
            The lowest cut-off frequency. The default value is 0.003 Hz.
        fmax : float
            The highest cut-off frequency. The default value is 0.03 Hz.

        Returns
        -------
        Callable
            A partial method call, where only coords keyword is missing.

        Example
        -------
        .. jupyter-execute::

            import pycoastalwater

            ds = pycoastalwater.tutorial.load_dataset("grid")
            ds.grid.hig()
        """
        method_kwargs = dict(
            df=df,
            fmin=fmin,
            fmax=fmax,
            ws_variable="hs",
            out_variable="hig",
            standard_name="sea_surface_ingragravity_wave_significant_height",
            units="m"
        )

        return partial(self.__compute_ws_variable, **method_kwargs)

    def hmax(self, coords):
        """
        Sea surface wave maximum height.

        Computed based on the maximum value of h.

        Returns
        -------
        ds : xarray.Dataset
            The coastal water dataset with the hmax data.
        coords : Optional[list[str, ...]]
            Which and how many coordinates to use. For example ["y", "x"].

        Example
        -------
        .. jupyter-execute::

            import pycoastalwater

            ds = pycoastalwater.tutorial.load_dataset("grid")
            ds.grid.hmax()
        """

        # compute hmax
        da = self._obj.h.max(dim="time")

        # copy obj
        ds = self._obj.copy()

        # add hmax as vars
        if coords is None:
            ds = ds.assign({"hmax": da.data})
        else:
            ds = ds.assign({"hmax": (coords, da.data,)})

        ds.hmax.attrs['standard_name'] = "sea_surface_wave_maximum_height"
        ds.hmax.attrs['units'] = "m"

        # float 16
        # ds = ds.astype('float16')

        return ds

    def hs(self, df=0.005, fmin=0.005, fmax=2) -> Callable:
        """
        Sea surface wave significant height.

        Computed based on a power density spectrum and a targetted frequency range.

        Parameters
        ----------
        df : float
            The df frequency required to compute the power density spectrum. The
            default value is 0.005 Hz.
        fmin : float
            The lowest cut-off frequency. The default value is 0.005 Hz.
        fmax : float
            The highest cut-off frequency. The default value is 2 Hz.

        Returns
        -------
        Callable
            A partial method call, where only coords keyword is missing.
        ds : xarray.Dataset
            The coastal water dataset with the hs data.

        Example
        -------
        .. jupyter-execute::

            import pycoastalwater

            ds = pycoastalwater.tutorial.load_dataset("grid")
            ds.grid.hs()
        """
        method_kwargs = dict(
            df=df,
            fmin=fmin,
            fmax=fmax,
            ws_variable="hs",
            out_variable="hs",
            standard_name="sea_surface_wave_significant_height",
            units="m"
        )

        return partial(self.__compute_ws_variable, **method_kwargs)

    def hsw(self, df=0.005, fmin=0.03, fmax=2) -> Callable:
        """
        Sea surface short wave significant height.

        Computed based on a power density spectrum and a targetted frequency range.

        Parameters
        ----------
        df : float
            The df frequency required to compute the power density spectrum. The
            default value is 0.005 Hz.
        fmin : float
            The lowest cut-off frequency. The default value is 0.03 Hz.
        fmax : float
            The highest cut-off frequency. The default value is 2 Hz.

        Returns
        -------
        Callable
            A partial method call, where only coords keyword is missing.
        ds : xarray.Dataset
            The coastal water dataset with the hsw data.

        Example
        -------
        .. jupyter-execute::

            import pycoastalwater

            ds = pycoastalwater.tutorial.load_dataset("grid")
            ds.grid.hsw()
        """
        method_kwargs = dict(
            df=df,
            fmin=fmin,
            fmax=fmax,
            ws_variable="hs",
            out_variable="hsw",
            standard_name="sea_surface_short_wave_significant_height",
            units="m"
        )

        return partial(self.__compute_ws_variable, **method_kwargs)

    def ef(self, df=0.005, coords=None):
        """
        Sea surface wave directional variance spectral density based on the Welch's method.

        Parameters
        ----------
        df : float
            The df frequency required to compute the power density spectrum. The
            default value is 0.005 Hz.
        coords : Optional[list[str, ...]]
            Which and how many coordinates to use. For example ["y", "x"].

        Returns
        -------
        ds : xarray.Dataset
            The coastal water dataset with the ef data.

        Example
        -------
        .. jupyter-execute::

            import pycoastalwater

            ds = pycoastalwater.tutorial.load_dataset("grid")
            ds.grid.ef()
        """

        # prepare data
        fs = 1 / (self._obj.time[1].data - self._obj.time[0].data)

        # fill nan values with bathy values, because detrend needs no nan values
        filled_data = self._obj['h'].combine_first(self._obj['bathy']).data

        # welch computations
        freq, ef = scipy.signal.welch(
            filled_data,
            nperseg=int(fs / df),
            axis=0,
            detrend="linear",
        )

        # copy obj
        ds = self._obj.copy()

        # add freq as coords
        ds = ds.assign_coords({"freq": (["freq"], freq)})
        ds.freq.attrs['standard_name'] = "sea_surface_wave_frequency"
        ds.freq.attrs['units'] = "Hz"

        # add ef as vars
        if coords is None:
            ds = ds.assign({"ef": (["freq"], ef)})
        else:
            ds = ds.assign({"ef": (["freq", *coords], ef)})
        ds.ef.attrs['standard_name'] = "sea_surface_wave_directional_variance_spectral_density"
        ds.ef.attrs['units'] = "m2 s degree-1"

        # float 16
        # ds = ds.astype('float16')

        return ds

    def ku(self, coords=None):
        """
        Sea surface kurtosis.

        Returns
        -------
        ds : xarray.Dataset
            The coastal water dataset with the ku data.
        coords : Optional[list[str, ...]]
            Which and how many coordinates to use. For example ["y", "x"].

        Example
        -------
        .. jupyter-execute::

            import pycoastalwater

            ds = pycoastalwater.tutorial.load_dataset("grid")
            ds.grid.ku()
        """

        # kurtosis computation
        ku = scipy.stats.kurtosis(self._obj['h'].data, axis=0)

        # copy obj
        ds = self._obj.copy()

        # add ku as var
        if coords is None:
            ds = ds.assign({"ku":  ku})
        else:
            ds = ds.assign({"ku": (coords, ku)})
        ds.ku.attrs['standard_name'] = "sea_surface_kurtosis"
        ds.ku.attrs['units'] = "m"

        # float 16
        # ds = ds.astype('float16')

        return ds

    def num_peaks(self, **kwargs):
        """
        Sea surface number of peaks based on peak properties.

        Parameters
        ----------
        kwargs
            Additional keyword arguments to be passed to the
            scipy.signal.find_peaks function.

        Returns
        -------
        ds : xarray.Dataset
            The coastal water dataset with the num_peaks data.

        Example
        -------
        .. jupyter-execute::

            import pycoastalwater

            ds = pycoastalwater.tutorial.load_dataset("grid")
            ds.grid.num_peaks()
        """
        # TODO: This might be generalizable for all datasets
        pass

    def setup(self, wl):
        """
        Sea surface setup.

        Parameters
        ----------
        wl : float
            The water level reference, which serve as reference to
            compute the setup.

        Returns
        -------
        ds : xarray.Dataset
            The coastal water dataset with the wl data.

        Example
        -------
        .. jupyter-execute::

            import pycoastalwater

            ds = pycoastalwater.tutorial.load_dataset("grid")
            ds.grid.setup(wl=4)
        """

        # copy obj
        ds = self._obj.copy()

        # setup
        ds["setup"] = ds["h"].mean(dim="time") - wl
        ds.setup.attrs['standard_name'] = "sea_surface_setup"
        ds.setup.attrs['units'] = "m"

        # setup formulation only below wl
        ds["setup"] = ds["setup"].where(ds["bathy"] < wl, np.nan)

        return ds

    def sk(self, coords=None):
        """
        Sea surface skewness.

        Returns
        -------
        ds : xarray.Dataset
            The coastal water dataset with the sk data.
        coords : Optional[list[str, ...]]
            Which and how many coordinates to use. For example ["y", "x"].

        Example
        -------
        .. jupyter-execute::

            import pycoastalwater

            ds = pycoastalwater.tutorial.load_dataset("grid")
            ds.grid.sk()
        """

        # skewness computation
        sk = scipy.stats.skew(self._obj['h'].data, axis=0)

        # copy obj
        ds = self._obj.copy()

        # add sk as var
        if coords is None:
            ds = ds.assign({"sk": sk})
        else:
            ds = ds.assign({"sk": (coords, sk)})

        ds.sk.attrs['standard_name'] = "sea_surface_skewness"
        ds.sk.attrs['units'] = "m"

        # float 16
        # ds = ds.astype('float16')

        return ds

    def tp(self, df=0.005, fmin=0.005, fmax=2) -> Callable:
        """
        Sea surface wave period at variance spectral density maximum.

        Computed based on a power density spectrum and a targetted frequency range.

        Parameters
        ----------
        df : int|float
            The df frequency required to compute the power density spectrum. The
            default value is 0.005 Hz.
        fmin : int|float
            The lowest cut-off frequency. The default value is 0.005 Hz.
        fmax : int|float
            The highest cut-off frequency. The default value is 2 Hz.

        Returns
        -------
        Callable
            A partial method call, where only coords keyword is missing.
        ds : xarray.Dataset
            The coastal water dataset with the tp data.

        Example
        -------
        .. jupyter-execute::

            import pycoastalwater

            ds = pycoastalwater.tutorial.load_dataset("grid")
            ds.grid.tp()
        """
        method_kwargs = dict(
            df=df,
            fmin=fmin,
            fmax=fmax,
            ws_variable="tp",
            out_variable="tp",
            standard_name="sea_surface_wave_period_at_variance_spectral_density_maximum",
            units="s"
        )

        return partial(self.__compute_ws_variable, **method_kwargs)

    def tm01(self, df=0.005, fmin=0.005, fmax=2) -> Callable:
        """
        Sea surface wave mean period.

        Computed from variance spectral density first frequency moment based
        on a power density spectrum and a targetted frequency range.

        Parameters
        ----------
        df : float
            The df frequency required to compute the power density spectrum. The
            default value is 0.005 Hz.
        fmin : float
            The lowest cut-off frequency. The default value is 0.005 Hz.
        fmax : float
            The highest cut-off frequency. The default value is 2 Hz.

        Returns
        -------
        Callable
            A partial method call, where only coords keyword is missing.
        ds : xarray.Dataset
            The coastal water dataset with the tm01 data.

        Example
        -------
        .. jupyter-execute::

            import pycoastalwater

            ds = pycoastalwater.tutorial.load_dataset("grid")
            ds.grid.tm01()
        """
        method_kwargs = dict(
            df=df,
            fmin=fmin,
            fmax=fmax,
            ws_variable="tm01",
            out_variable="tm01",
            standard_name="sea_surface_wave_mean_period_from_variance_spectral_density_first_frequency_moment",
            units="s"
        )

        return partial(self.__compute_ws_variable, **method_kwargs)

    def tm02(self, df=0.005, fmin=0.005, fmax=2) -> Callable:
        """
        Sea surface wave mean period.

        Computed from variance spectral density second frequency moment based
        on a power density spectrum and a targetted frequency range.

        Parameters
        ----------
        df : float
            The df frequency required to compute the power density spectrum. The
            default value is 0.005 Hz.
        fmin : float
            The lowest cut-off frequency. The default value is 0.005 Hz.
        fmax : float
            The highest cut-off frequency. The default value is 2 Hz.

        Returns
        -------
        Callable
            A partial method call, where only coords keyword is missing.
        ds : xarray.Dataset
            The coastal water dataset with the tm02 data.

        Example
        -------
        .. jupyter-execute::

            import pycoastalwater

            ds = pycoastalwater.tutorial.load_dataset("grid")
            ds.grid.tm02()
        """
        method_kwargs = dict(
            df=df,
            fmin=fmin,
            fmax=fmax,
            ws_variable="tm02",
            out_variable="tm02",
            standard_name="sea_surface_wave_mean_period_from_variance_spectral_density_second_frequency_moment",
            units="s"
        )

        return partial(self.__compute_ws_variable, **method_kwargs)

    def ws(self):
        """
        Sea surface water sheet crest height.

        Returns
        -------
        ds : xarray.Dataset
            The coastal water dataset with the ws data.

        Example
        -------
        .. jupyter-execute::

            import pycoastalwater

            ds = pycoastalwater.tutorial.load_dataset("grid")
            ds.grid.ws()
        """

        # copy obj
        ds = self._obj.copy()

        # ws
        ds["ws"] = ds["h"] - ds["bathy"]
        ds.ws.attrs['standard_name'] = "sea_surface_water_sheet_crest_height"
        ds.ws.attrs['units'] = "m"

        # float 16
        # ds = ds.astype('float16')

        return ds

    def ws_q(self, q, coords=None):
        """
        Crest height of the sea surface water sheet qth quantile.

        Parameters
        ----------
        q : float
            The qth quantile to compute, which must be between 0 and 1 inclusive.
        coords : Optional[list[str, ...]]
            Which and how many coordinates to use. For example ["y", "x"].

        Returns
        -------
        ds : xarray.Dataset
            The coastal water dataset with the ws_q data.

        Example
        -------
        .. jupyter-execute::

            import pycoastalwater

            ds = pycoastalwater.tutorial.load_dataset("grid")
            ds.grid.ws_q(q=0.75)
        """

        # copy obj
        ds = self._obj.copy()

        # lde
        ws = ds["h"] - ds["bathy"]

        # lde quantile
        ws_q = ws.quantile(q, dim="time")

        # add lde_q as var
        if coords is None:
            ds = ds.assign({"ws_q": ws_q.data})
        else:
            ds = ds.assign({"ws_q": (coords, ws_q.data,)})

        ds.ws_q.attrs['standard_name'] = "sea_surface_water_sheet_crest_height_quantile"
        ds.ws_q.attrs['units'] = "m"

        # float 16
        # ds = ds.astype('float16')

        return ds