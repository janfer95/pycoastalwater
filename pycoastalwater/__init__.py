""" 
PyCoastalWater is the fundamental package to manipulate coastal water data using xarray.
"""

__version__ = "0.3.1"

from .pointdataset import PointDataset
from .linedataset import LineDataset
from .griddataset import GridDataset
from pycoastalwater import tutorial