Tutorial Save
=============

Import python package.

.. jupyter-execute::

    import holoviews as hv
    import pycoastalwater

Load the tutorial coastal water xarray dataset on a point.

.. jupyter-execute::

    ds = pycoastalwater.tutorial.load_dataset("grid")

Create a nice bathymetry map plot.

.. jupyter-execute::

    plot = ds.where(ds.bathy<0).grid.map("bathy", cmap="jet", clim=(-15, 0))
    plot.opts(title="Bathymetry [m]", width=500, height=500)

Save the plot as a png image. ::

    hv.save(plot, "image.png")

Save the plot as an interactive html file. ::

    hv.save(plot, "image.html")

The surface height data are referenced to the chart datum.
This reference can be changed to the associated tide water level of +4m.

.. jupyter-execute::

    ds["h"] = ds["h"] - 4
    ds.h.attrs["standard_name"] = "sea_surface_height_above_tide_water_level"
    ds.h.attrs["units"] = "m"

Create a nice sea surface height animated map plot.

.. jupyter-execute::

    plot = ds.sel(time=slice(0, 30)).grid.map("h")
    plot.opts(width=500, height=500)

Save the plot as a GIF file. ::

    hv.save(plot, "animation.gif")

Save the plot as an interactive html file. ::

    hv.save(plot, "animation.html")
