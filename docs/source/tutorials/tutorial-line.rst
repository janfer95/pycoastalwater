Tutorial Line
=============

Import python package.

.. jupyter-execute::

    import pycoastalwater

Load the tutorial coastal water xarray dataset on a line.

.. jupyter-execute::

    ds = pycoastalwater.tutorial.load_dataset("line")

Plot the bathymetry on a map.

.. jupyter-execute::

    ds.line.map("bathy", cmap="jet")

Plot the sea surface height between 0 and 30 seconds on a map.

.. jupyter-execute::

    ds.sel(time=slice(0, 30)).line.map("h")

Plot the sea surface height between 0 and 30 seconds, with the bathymetry.

.. jupyter-execute::

    plot = ds.sel(time=slice(0, 30)).line.plot("h", x="xm", ylim=(-15, 10), label="h")
    plot = plot * ds.line.plot("bathy", x="xm", color="grey", ylim=(-15, 10), label="bathy")
    plot.opts(legend_position='bottom_right')

Compute hs, hsw and hig.

.. jupyter-execute::

    ds = ds.line.hs().line.hsw().line.hig()

Plot hs, hsw and hig on a map.

.. jupyter-execute::

    (ds.line.map("hs") + ds.line.map("hsw") + ds.line.map("hig")).cols(1)

Plot hs, hsw, hig and bathy.

.. jupyter-execute::

    plot = ds.line.plot("hs", x="xm", color="blue", label="hs")
    plot = plot * ds.line.plot("hsw", x="xm", color="black", label="hsw")
    plot = plot * ds.line.plot("hig", x="xm", color="red", label="hig")
    plot = plot + ds.line.plot("bathy", x="xm", color="grey")
    plot.cols(1)

Compute and plot runup positions between 0 and 30 seconds on a map.

.. jupyter-execute::

    ds.sel(time=slice(0, 30)).line.runup().line.map('runup')

Compute and plot runup positions between 0 and 30 seconds.

.. jupyter-execute::

    plot = ds.sel(time=slice(0, 30)).line.plot('h', x="xm", label="h", ylim=(-5, 15))
    plot = plot * ds.sel(time=slice(0, 30)).line.runup().line.plot(
        'runup', plot_type="scatter", x="xm", label="runup", color="red")
    plot = plot * ds.line.plot('bathy', x="xm", label="bathy", color="grey")
    plot.opts(xlim=(2100, 2410))
    
Compute and plot on a map runup positions 75th quantile.

.. jupyter-execute::

    ds.line.runup_q(q=0.75).line.map('runup_q')

Compute number of peaks ~= number of waves for the selected upper beach area. 
Use physical definiton to identify a peak wave with a required minimum height of 0.2 m, 
and a minimum distance of 8 sec.

.. jupyter-execute::

    dt = ds.time[1] - ds.time[0] 
    ds.sel(x=slice(40, 80)).line.num_peaks(height=0.2, distance=int(8)).line.map('num_peaks')
