Tutorial Grid
=============

Import python package.

.. jupyter-execute::

    import pycoastalwater

Load the tutorial coastal water xarray dataset on a grid.

.. jupyter-execute::

    ds = pycoastalwater.tutorial.load_dataset("grid")

Plot the bathymetry on a map.

.. jupyter-execute::

    ds.grid.map("bathy", cmap="jet")

Plot the sea surface height between 0 and 30 seconds on a map.

.. jupyter-execute::

    ds.sel(time=slice(0, 30)).grid.map("h")

Compute hs, hsw and hig.

.. jupyter-execute::

    ds = ds.grid.hs().grid.hsw().grid.hig()

Plot hs, hsw and hig on a map.

.. jupyter-execute::

    (ds.grid.map("hs") + ds.grid.map("hsw") + ds.grid.map("hig")).cols(1)

Compute and plot on a map runup positions between 0 and 30 seconds, 
for a selected area of x and y.

.. jupyter-execute::

    ds.sel(time=slice(0, 30), x=slice(50, 60), y=slice(20, 90)).grid.runup().grid.map('runup')

Add the associated surface height data.

.. jupyter-execute::

    plot = ds.sel(time=slice(0, 30), x=slice(50, 60), y=slice(20, 90)).grid.map('h')
    plot = plot * ds.sel(time=slice(0, 30), x=slice(50, 60), y=slice(20, 90)).grid.runup().grid.map('runup', cmap="reds")
    plot

Compute and plot on a map runup positions 75th quantile.

.. jupyter-execute::

    ds.sel(x=slice(50, 60), y=slice(20, 90)).grid.runup_q(q=0.75).grid.map('runup_q')

Compute number of peaks ~= number of waves for the selected upper beach area. 
Use physical definiton to identify a peak wave with a required minimum height of 0.2 m, 
and a minimum distance of 8 sec.

.. jupyter-execute::

    dt = ds.time[1] - ds.time[0] 
    ds.sel(x=slice(55, 60), y=slice(20, 90)).grid.num_peaks(height=0.2, distance=int(8/dt)).grid.map('num_peaks')
