Tutorial Create
===============

Coastal water xarray can be created from scratch.

Create grid
-----------

Import python package.

.. jupyter-execute::

    import pycoastalwater
    import xarray as xr

Load the tutorial coastal water xarray dataset on a grid.

.. jupyter-execute::

    ds = pycoastalwater.tutorial.load_dataset("grid")

Extract data as numpy arrays.

.. jupyter-execute::

    time = ds.time.data
    x = ds.x.data
    y = ds.y.data
    h = ds.h.data
    bathy = ds.bathy.data

Create a coastal water xarray from the numpy arrays.

.. jupyter-execute::

    ds2 = xr.Dataset(
        data_vars=dict(
            bathy=(["y", "x"], bathy, {"standard_name":"sea_floor_depth_below_chart_datum", "units":"m"}),
            h=(["time", "y", "x"], h, {"standard_name":"sea_surface_height_above_chart_datum", "units":"m"}),
        ),
        coords=dict(
            time=(["time"], time, {"standard_name":"time", "units":"s"}),
            y=(["y"], y, {"standard_name":"y", "units":"logical_coordinates"}),
            x=(["x"], x, {"standard_name":"x", "units":"logical_coordinates"}),
        ),
    )
    print(ds2)

Additional data can be latter added to the coastal water xarray.

.. jupyter-execute::

    ds2 = ds2.assign({"hmax": ds2.h.max(dim="time")})
    ds2.hmax.attrs['standard_name'] = "sea_surface_wave_maximum_height"
    ds2.hmax.attrs['units'] = "m"
    print(ds2)

Plot the added hmax data.

.. jupyter-execute::

    ds2.grid.plot("hmax")

Create line
-----------

Load the tutorial coastal water xarray dataset on a line.

.. jupyter-execute::

    ds = pycoastalwater.tutorial.load_dataset("line")

Extract data as numpy arrays.

.. jupyter-execute::

    time = ds.time.data
    x = ds.x.data
    h = ds.h.data
    bathy = ds.bathy.data

Create a coastal water xarray from the numpy arrays.

.. jupyter-execute::

    ds2 = xr.Dataset(
        data_vars=dict(
            bathy=(["x"], bathy, {"standard_name":"sea_floor_depth_below_chart_datum", "units":"m"}),
            h=(["time", "x"], h, {"standard_name":"sea_surface_height_above_chart_datum", "units":"m"}),
        ),
        coords=dict(
            time=(["time"], time, {"standard_name":"time", "units":"s"}),
            x=(["x"], x, {"standard_name":"x", "units":"logical_coordinates"}),
        ),
    )
    print(ds2)

Create point
------------

Load the tutorial coastal water xarray dataset on a point.

.. jupyter-execute::

    ds = pycoastalwater.tutorial.load_dataset("point")

Extract data as numpy arrays.

.. jupyter-execute::

    time = ds.time.data
    h = ds.h.data
    bathy = ds.bathy.data

Create a coastal water xarray from the numpy arrays.

.. jupyter-execute::

    ds2 = xr.Dataset(
        data_vars=dict(
            bathy=([], bathy, {"standard_name":"sea_floor_depth_below_chart_datum", "units":"m"}),
            h=(["time"], h, {"standard_name":"sea_surface_height_above_chart_datum", "units":"m"}),
        ),
        coords=dict(
            time=(["time"], time, {"standard_name":"time", "units":"s"}),
        ),
    )
    print(ds2)
