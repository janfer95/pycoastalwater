Tutorial Point
==============

Import python package.

.. jupyter-execute::

    import pycoastalwater

Load the tutorial coastal water xarray dataset on a point.

.. jupyter-execute::

    ds = pycoastalwater.tutorial.load_dataset("point")

Plot the bathymetry on a map.

.. jupyter-execute::

    ds.point.map("bathy", cmap="jet")

Plot the sea surface height between 0 and 30 seconds on a map.

.. jupyter-execute::

    ds.sel(time=slice(0, 30)).point.map("h")

Plot the sea surface height.

.. jupyter-execute::

    ds.point.plot("h")

Compute and plot surface height density spectra.

.. jupyter-execute::

    ds.point.ef().point.plot("ef")

Compute hs, hsw and hig.

.. jupyter-execute::

    ds = ds.point.hs().point.hsw().point.hig()

Plot the surface height density spectra with hs, hsw and hig on a values.

.. jupyter-execute::

    plot = ds.point.ef().point.plot("ef")
    title = "Hs=" + '{:.2f}'.format(ds.hs.data) + \
        "m, Hsw="'{:.2f}'.format(ds.hsw.data) + \
        "m, Hig="'{:.2f}'.format(ds.hig.data)
    plot.opts(title=title)

Compute number of peaks ~= number of waves. Use physical definiton to identify 
a peak wave with a required minimum height of 0.2 m, and a minimum distance of 8 sec.

.. jupyter-execute::

    dt = ds.time[1] - ds.time[0] 
    ds = ds.point.num_peaks(height=0.2, distance=int(8))

Plot the sea surface height with the flagged peaks.

.. jupyter-execute::

    plot = ds.point.plot("h")
    plot = plot * ds.sel(time=ds.time_peak).point.plot("h", plot_type="scatter", color="red")
    plot
