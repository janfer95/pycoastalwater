Pycoastalwater documentation
============================

Pycoastalwater is a powerfull and elegant tool to work with coastal water data.

.. toctree::
   :hidden:

   getting-started-guide/index 
   tutorials/index
   api

.. image:: logo.png
   :width: 600
