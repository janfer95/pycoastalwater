Getting Started
===============

The getting started guide helps you understand how to use pycoastalwater.

.. toctree::
   :hidden:

   overview
   installing