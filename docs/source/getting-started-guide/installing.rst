Installing
==========

Instructions
------------

Install required dependencies using conda. ::
    
    $ conda install -c conda-forge cartopy colorcet datashader h5netcdf geopandas geoviews git hvplot netcdf4 numpy pandas pip plotly scipy shapely wavespectra xarray

Install pycoastalwater using pip. ::
    
    $ pip install git+https://gitlab.com/kostarisk/pycoastalwater


Detailled instructions
----------------------

Install anaconda from https://anaconda.org/.

Open the anaconda prompt.

Inside the anaconda prompt, create a new conda env. ::

    $ conda create --name MyEnvironmentName

Activate the new env. ::

    $ conda activate MyEnvironmentName

Install required dependencies using conda. ::
    
    $ conda install -c conda-forge cartopy colorcet datashader h5netcdf geopandas geoviews git hvplot netcdf4 numpy pandas pip plotly scipy shapely wavespectra xarray

Install pycoastalwater using pip. ::
    
    $ pip install git+https://gitlab.com/kostarisk/pycoastalwater