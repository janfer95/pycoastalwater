Overview
========

Why pycoastalwater ?
------------------

Pycoatalwater extends coastal water xarray to play easily with coastal 
water data and associated parameters.

Coastal water
-------------

Coastal water refers to the areas of water that are adjacent to coastlines, 
typically extending from the shoreline to a certain distance offshore. It 
encompasses the transitional zone between the land and the open sea. 
Coastal waters can include a variety of environments such as bays, estuaries, 
lagoons, and nearshore areas.

Coastal water is characterized by both its bathymetry (underwater topography) 
and sea surface height. Bathymetry provides information about the shape and 
features of the seafloor, while sea surface height captures variations in the 
vertical distance between average sea level and the actual height of the water 
surface. Understanding both aspects is crucial for studying coastal dynamics 
and processes, including wave propagations, runup and coastal surges.

Coastal water xarray
--------------------

A coastal water xarray can be seen as an xarray containing bathymetry data 
and sea surface height data, as well as their associated coordinates and related 
parameters. Pycoastalwater extends these xarray with specific methods : 
for easily computing of related parameters and for easily visualizations.

Pycoastalwater objects
----------------------

A coastal water xarray can be differenciated based on its spatial configuration : 
data at a point, data along a line or data accross a grid. Several types of 
pycoastalwater objects are created to extend each of these specific coastal water 
xarray :

- PointDataset : to extend coastal water xarray with data at a point,
- LineDataset : to extend coastal water data along a line,
- GridDataset : to extend coastal water data at a grid in space.

Goals and aspirations
---------------------

Pycoastalwater contributes to better and easily understanding of observed or 
modelled coastal water data.

It goals is to become a community driven project to help better address 
the numerous challenges faced by coastal areas.