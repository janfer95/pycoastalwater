API reference
=============

This reference manual details functions, modules, and objects included in pycoastalwater, 
describing what they are and what they do. 

.. currentmodule:: pycoastalwater

pycoastalwater
--------------
.. autosummary::
   :nosignatures:
   :toctree: generated/

   PointDataset
   LineDataset
   GridDataset

pycoastalwater.PointDataset
---------------------------
.. autosummary::
   :nosignatures:
   :toctree: generated/

   PointDataset.hig
   PointDataset.hmax
   PointDataset.hs
   PointDataset.hsw
   PointDataset.ef
   PointDataset.ku
   PointDataset.map
   PointDataset.num_peaks
   PointDataset.plot
   PointDataset.setup
   PointDataset.sk
   PointDataset.tp
   PointDataset.tm01
   PointDataset.tm02
   PointDataset.ws
   PointDataset.ws_q

pycoastalwater.LineDataset
---------------------------
.. autosummary::
   :nosignatures:
   :toctree: generated/

   LineDataset.hig
   LineDataset.hmax
   LineDataset.hs
   LineDataset.hsw
   LineDataset.ef
   LineDataset.ku
   LineDataset.map
   LineDataset.num_peaks
   LineDataset.plot
   LineDataset.runup
   LineDataset.setup
   LineDataset.sk
   LineDataset.tp
   LineDataset.tm01
   LineDataset.tm02
   LineDataset.ws
   LineDataset.ws_q

pycoastalwater.GridDataset
---------------------------
.. autosummary::
   :nosignatures:
   :toctree: generated/

   GridDataset.hig
   GridDataset.hmax
   GridDataset.hs
   GridDataset.hsw
   GridDataset.ef
   GridDataset.ku
   GridDataset.map
   GridDataset.map_image
   GridDataset.num_peaks
   GridDataset.plot
   GridDataset.runup
   GridDataset.setup
   GridDataset.sk
   GridDataset.tp
   GridDataset.tm01
   GridDataset.tm02
   GridDataset.ws
   GridDataset.ws_q
   