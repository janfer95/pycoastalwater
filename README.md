Pycoastalwater project
======================

Pycoastalwater is a powerfull and elegant tool to work with coastal water data.

Documentation
-------------
The documentation is hosted on ReadTheDocs at https://pycoastalwater.readthedocs.io/en/latest/.

Install
-------
The installation steps are described at https://pycoastalwater.readthedocs.io/en/latest/install.html

Main developper
---------------
Danglade, Nikola : nikola.danglade@suez.com